package zoomcar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC001_ZoomCar extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_ZoomCar";
		testCaseDesc="Book a car in ZOOM Car";
		category="Smoke";
		author="Anandhi & Jeeva";
	}

	@Test
	public void bookCar() throws InterruptedException {	
		startApp("chrome", "https://www.zoomcar.com/chennai");

		WebElement search=locateElement("xpath","//a[text()='Start your wonderful journey']");
		click(search);

		WebElement pickUpPoint=locateElement("xpath","//div[@class='component-popular-locations']/div[2]");
		click(pickUpPoint);

		WebElement btnNext=locateElement("xpath","//button[text()='Next']");
		click(btnNext);

		// Get the current date
		Date date = new Date();
		// Get only the date (and not month, year, time etc)
		DateFormat sdf = new SimpleDateFormat("dd"); 
		// Get today's date
		String today = sdf.format(date);
		// Convert to integer and add 1 to it
		int tomorrow = Integer.parseInt(today)+1;
		// Print tomorrow's date
		
		System.out.println(tomorrow);
		WebElement dayPicked=locateElement("xpath","//div[@class='day' and contains(text(),'17')]");
		click(dayPicked);
		
		WebElement btnNext1=locateElement("xpath","//button[text()='Next']");
		click(btnNext1);
		
		WebElement dayPicked1=locateElement("xpath","//div[@class='day picked ' and contains(text(),'17')]");
		verifySelected(dayPicked1);
		
		WebElement btnDone=locateElement("xpath","//button[text()='Done']");
		click(btnDone);
				
		List<WebElement> carList = driver.findElementsByXPath("//div[@class='car-list-layout']/div[*]");
		int size = carList.size();
		System.out.println(size);
		
		for(WebElement i:carList)
		{
			System.out.println(i.getText());
		}
		
		List<WebElement> carPriceList=driver.findElementsByXPath("//div[@class='car-list-layout']/div[*]//div[@class='price']");
		
		List<Integer> priceVal=new ArrayList<Integer>();
		for(WebElement i:carPriceList)
			
		{
			String text = i.getText();
			//REPLACE(oldText, startNum, numChars, newText)
			String replaceAll = text.replaceAll("\\D", "");
			System.out.println(replaceAll);
			int parseInt = Integer.parseInt(replaceAll);
			priceVal.add(parseInt);
						
		}
		
		Integer max = Collections.max(priceVal);
		System.out.println("Max Price= "+max);
		
		WebElement brandName = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/preceding::h3[1]");
		getText(brandName);
		
		WebElement btnBook = driver.findElementByXPath("//div[contains(text(),'"+max+"')]/following::div/button");
		click(btnBook);
		Thread.sleep(6000);
		
	}

}
