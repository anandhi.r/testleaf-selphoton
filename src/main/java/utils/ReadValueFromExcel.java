package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public  class ReadValueFromExcel {

	public static Object[][] readExcelData(String fName) throws IOException 
	{
		XSSFWorkbook wBook=new XSSFWorkbook("./data/"+fName+".xlsx");
		XSSFSheet sheet = wBook.getSheetAt(0);
		
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row Count is= " +rowCount);
		
		short columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column Count= " +columnCount);
		Object[][] data=new Object[rowCount][columnCount];
		
		for (int j = 1; j <= rowCount; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i =0 ; i < columnCount; i++) {
				XSSFCell cell = row.getCell(i);
				data[j-1][i] = cell.getStringCellValue();
				//System.out.println(stringCellValue);
			} 
		}
		return data;
		
	}

	
}
