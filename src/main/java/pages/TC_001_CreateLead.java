package pages;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdmethods.ProjectMethods;

public class TC_001_CreateLead extends ProjectMethods {
	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testCaseName = "LearnPages";
		testCaseDesc="Create a New Lead";
		category="Smoke";
		author="Anandhi";
		fileName="projectData";
	}

	
	@Test(dataProvider="fetchData",groups= {"smoke"})
	public void createLead(String usName, String passWord, String cName, String fName,String fLName) {
		new LoginPage().typeUserName(usName).typePassWord(passWord).clickLogin().clickCRMSFA()
	.clickLead().clickCreateLead()
	.typecomNameEle(cName).typeFNameEle(fName).typeLNameEle(fLName).clickSubmit().verifyLead(fName);
		
	}
}
