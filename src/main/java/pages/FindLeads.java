package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class FindLeads extends ProjectMethods {

	public FindLeads() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(xpath="(//div[@class='gwtWidget subSectionBlock']//input)[2]")
	WebElement elefindLeadViaName;
	public FindLeads typeFindLeadViaName(String name) {
		type(elefindLeadViaName, name);
		return this;
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleFindLeadBtn;
	public FindLeads clickFindLeadsBtn() {
		click(eleFindLeadBtn);
		return this;
	}
	
}
