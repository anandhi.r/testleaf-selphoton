package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class MyHomePage extends ProjectMethods{

	public MyHomePage() {
		PageFactory.initElements(driver, this);
	}

	
	@FindBy(linkText="Leads")
	WebElement eleCreateLead;
	public MyLeads clickLead() {
		click(eleCreateLead);
		return new MyLeads();
	}
	

}
