package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class MyLeads extends ProjectMethods{

	public MyLeads() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(linkText="Create Lead")
	WebElement eleLead;
	public CreateLeadPage clickCreateLead() {
		click(eleLead);
		return new CreateLeadPage();
	}
	
	@FindBy(linkText="Find Leads")
	WebElement eleFindLead;
	public FindLeads clickFindLead() {
		click(eleFindLead);
		return new FindLeads();
	}
}
