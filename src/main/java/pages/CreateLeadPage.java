package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public  class  CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
		
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCName;
	public CreateLeadPage typecomNameEle(String cName) {
		type(eleCName, cName);
		return this;
	} 

	@FindBy(id="createLeadForm_firstName")
	WebElement eleFName;
	public CreateLeadPage typeFNameEle(String FName) {
		type(eleFName, FName);
		return this;
	}

	@FindBy(id="createLeadForm_lastName")
	WebElement eleLName;
	public CreateLeadPage typeLNameEle(String LName) {
		type(eleLName, LName);
		return this;
	}
	@FindBy(className="smallSubmit")
	WebElement eleSubmit;
	public ViewLead clickSubmit() {
		click(eleSubmit);
		return new ViewLead();
	} 

}
