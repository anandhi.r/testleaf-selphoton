package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class ViewLead extends ProjectMethods{
	
	public ViewLead() {
		
		PageFactory.initElements(driver, this);
	} 
	
	@FindBy(xpath="//span[text()='jj']")
	WebElement verifyFName;
	public ViewLead verifyLead(String FName) {
		verifyExactText(verifyFName, FName);
		return this;
	}
	
	
}
