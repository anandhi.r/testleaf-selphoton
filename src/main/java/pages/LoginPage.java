package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class LoginPage extends ProjectMethods {
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="username")
	WebElement eleUserName;
	public LoginPage typeUserName(String userName) {
		//WebElement eleUserName=locateElement("id", "username");
		type(eleUserName, userName);
		return this;
	}
	
	@FindBy(id="password")
	WebElement elepassWord;
	public LoginPage typePassWord(String passWord) {
		type(elepassWord, passWord);
		return this;
	}
	
	@FindBy(className="decorativeSubmit")
	WebElement eleLogin;
	public HomePage clickLogin() {
		click(eleLogin);
		
		return new HomePage();
	}

}
