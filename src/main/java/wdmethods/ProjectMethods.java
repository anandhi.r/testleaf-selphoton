package wdmethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import utils.ReadValueFromExcel;

public class ProjectMethods extends SeMethods{
	@BeforeSuite(groups= {"common"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups= {"common"})
	public void beforeClass() {
		startTestCase();
	}
	
	//@Parameters({"url"})
	@BeforeMethod(groups= {"common"})
	public void login(){
		startApp("chrome", "http://leaftaps.com/opentaps/");
		
	}
	
	/*//@Parameters({"url","username","password"})
	@BeforeMethod(groups= {"common"})
	public void login(String username,String password){
		startApp("chrome", "http://leaftaps.com/opentaps/");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("id","password");
		type(elePassword, password);
	//	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	//	click(eleLogin);
	//	WebElement eleCRM = locateElement("linktext","CRM/SFA");
	//	click(eleCRM);
	}

	/* 
	 * @BeforeMethod

	public void login() {
		startApp("chrome", "https://www.facebook.com/");
		WebElement eleUserName = locateElement("xpath", "//input[@id='email']");
		type(eleUserName, "anz2k10@gmail.com");
		WebElement elePassword = locateElement("xpath","//input[@id='pass']");
		type(elePassword, "visteon2676");
		WebElement eleLogin = locateElement("xpath", "//label[@id='loginbutton']");
		click(eleLogin);

	}
	 */

	@AfterMethod(groups= {"common"})
	public void closeApp(){
		closeBrowser();
	}
	@AfterSuite(groups= {"common"})
	public void afterSuite() {
		endResult();
	}
	
	@DataProvider(name="fetchData")
	public Object[][] fetchData() throws IOException
	{
		//ReadValueFromExcel obj1=new ReadValueFromExcel();
		//Object[][] data = ReadValueFromExcel.readExcelData();
		return ReadValueFromExcel.readExcelData(fileName);
		
	}
}
