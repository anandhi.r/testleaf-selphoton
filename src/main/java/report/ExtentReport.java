package report;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {

	public static void main(String[] args) throws IOException {
		
		ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		
		//override existing file if it is false
		html.setAppendExisting(true);
		
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest test = extent.createTest("TC001_CreateLead", "Create New Lead");
		
		test.assignCategory("Smoke Test");
		test.assignAuthor("Anandhi");
		
		//Test Case Report Step Level
		
		test.pass("The Browser Launched Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		test.pass("The data DemoSalesManager enetered Successfully");
		test.fail("The data crmsfa not entered Successfully", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
		
		extent.flush();

	}

}
